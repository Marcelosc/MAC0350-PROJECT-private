# MAC0350-PROJECT

## 1 - Sobre a nossa entrega:

### a - Alunos

* **André Luiz Abdalla Silveira 8030353**
* **Marcelo Schmitt 9297641**

### b - Observações 

Tudo o que está acima, já vinha com o repositório original. Aqui
gostaríamos de destacar alguns detalhes sobre nossa implementação:
* Na pasta **/server/script**, há dois scripts para inserção de dados 
    * _sample-data.sql_ -- popular o BD
    * _additional-data.sql_  -- acrescentar mais dados
    * _delete-all.sql_ -- para deletar as entradas 
    * Tais arquivos devem ser executados dentro do shell do 
    postgreSQL
        * ```dbz=# \i scripts/<arquivo>.sql ```

Na próxima sessão encontram-se algumas instruções de como rodar.

## 2 - Instruções de execução

Template for the project developed during the course MAC0350
(Introduction to Systems Development) at IME-USP.

This repository is a monorepo and requires [docker][1] and
[docker-compose][2] to run the services.

In order to setup the back-end services, open a shell and run:
> ```bash
> cd server
> docker-compose up
> ```

In order to setup the front-end services, open another shell and run:
> ```bash
> cd client
> docker-compose up
> ```

[1]: https://store.docker.com/search?type=edition&offering=community
[2]: https://docs.docker.com/compose/install/ 
