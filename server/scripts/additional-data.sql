--------------------------------------------------------------------------------
--- Insere disciplinas
--------------------------------------------------------------------------------

--Delete para não gerar duas disciplinas com código MAC0121
DELETE FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0121';
--Delete para não gerar duas disciplinas com código MAC0110
DELETE FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0110';

INSERT INTO
public.disciplinas(
  codigo_disciplina,
  creditos_aula,
  creditos_trabalho,
  carga_horaria,
  ativacao,
  nome,
  ementa)
VALUES
-- ( 'MAC0101', 4, 0, 60, '2015-01-01' ),
-- ( 'MAC0110', 4, 0, 60, '1998-01-01' ),
-- ( 'MAC0121', 4, 0, 60, '2015-01-01' ),
('MAC0110', 4, 0, 60, '1998-01-01', 'Introdução à Computação', 'Breve história da computação. Algoritmos: caracterização, notação, estruturas básicas. Computadores: unidades básicas, instruções, programa armazenado, endereçamento, programas em linguagem de máquina. Conceitos de linguagens algorítmicas: expressões; comandos seqüenciais, seletivos e repetitivos; entrada/saída; variáveis estruturadas;'),
('MAC0329', 4, 0, 60, '2013-01-01', 'Álgebra Booleana e Circuitos Digitais', 'Sistemas de representação numérica: bases binária, octal e hexadecimal, conversão entre bases, aritmética com números binários. Noções de circuitos lógicos: funções lógicas, tabelas-verdade, portas lógicas. Noções de organização de computadores.'),
('MAC0121', 4, 0, 60, '2013-01-01', 'Algoritmos e Estruturas de Dados I', 'Noções de correção e desempenho de algoritmos. Noções de tipos abstratos de dados. Vetores e matrizes. Alocação dinâmica de memória. Apontadores. Listas ligadas.'),
('MAC0323', 4, 0, 60, '2013-01-01', 'Algoritmos e Estruturas de Dados II', 'Tipos abstratos de dados e suas implementações. Tabelas de símbolos: árvores de busca balanceadas, tabelas de espalhamento (hashing).'),
('MAC0239', 4, 0, 60, '2013-01-01', 'Introdução à Lógica e Verificação de Programas', 'Lógica Proposicional Clássica: Os conectivos Booleanos e a linguagem da LPC. Semântica clássica. Tabelas da Verdade. Implicação Lógica. Equivalência Lógica e formas normais. Métodos de prova e inferência lógica, tais como Axiomatizações, Dedução Natural e métodos de Tableaux.'),
('MAC0216', 4, 2, 60, '2013-01-01', 'Técnicas de Programação I', 'Arquitetura de computadores. Linguagem de montagem e ligação de código objeto. Interação com o sistema operacional e Shell scripts. Gerenciamento de compilação de programas Modularização de código. Bibliotecas estáticas e dinâmicas.'),
('MAC0218', 4, 2, 60, '2015-01-01', 'Técnicas de Programação II', 'Controle de versões tais como git e boas práticas metodológicas para desenvolvimento de software de forma colaborativa. Princípios de orientação a objetos: encapsulamento, herança, polimorfismo, interfaces, tratamento de exceções.'),
('MAC0316', 4, 0, 60, '2015-01-01', 'Conceitos Fundamentais de Linguagens de Programação', 'Linguagens funcionais. Funções como valores de primeira ordem; polimorfismo; ambientes e fechamentos; avaliação "adiada"; linguagens funcionais; linguagens orientadas a objetos. Tipos abstratos de dados. Módulos. Herança e hierarquias.'),
('MAC0209', 4, 0, 60, '2015-01-01', 'Modelagem e Simulação', 'Estudo de aspectos da pesquisa científica e seus métodos. Realização de experimentos simples em sala de aula, conceituação de medidas, leis, corroboração, falseamento, levando à modelagem matemática, determinística e estocástica de sistemas físicos. Realização computacional de modelos, com simulações apresentadas na forma analítica e de animações gráficas, em ambientes computacionais especializados para simulação e animação.'),
('MAC0219', 4, 0, 60, '2010-01-01', 'Programação Concorrente e Paralela', 'Conceitos básicos: processos, threads, interrupções, escalonamento. Problemas de programação concorrente: deadlock, alocação de recursos, leitura e escrita concorrente, exclusão mútua, consenso. Programação concorrente em UNIX.'),
('MAC0460', 4, 0, 60, '2015-01-01', 'Aprendizagem Computacional: Modelos, Algoritmos e Aplicações', 'Conceitos, hipóteses e algoritmos de aprendizagem. Representações e fórmulas booleanas. Decomposições por Morfologia Matemática. Decomposições por Redes Neurais. Aprendizagem probabilística. Aprendizagem eficiente. Dimensão VC. Aplicações.')
-- ( 'MAC0350', 4, 0, 60, '2017-06-01' ),
-- ( 'MAC0426', 4, 0, 60, '1998-01-01' );
;

--------------------------------------------------------------------------------
-- Inserção de pré-requisitos
--------------------------------------------------------------------------------

INSERT INTO public.prerequisitos(
  id_disciplina,
  id_prerequisito,
  total
) 
VALUES
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0426'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0350'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0350'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0316'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0460'), 
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0329'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0219'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0121'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0219'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0216'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0209'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0110'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0316'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0121'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0218'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0216'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0216'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0110'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0239'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0110'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0323'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0121'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0323'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0216'),
    True
  ),
  (
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0121'),
    (SELECT id_disciplina FROM public.disciplinas WHERE disciplinas.codigo_disciplina = 'MAC0110'),
    True
  )
;
