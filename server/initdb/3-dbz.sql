-- Default schema for the business logic
CREATE SCHEMA IF NOT EXISTS public;

--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS
public.disciplinas (
  id_disciplina      bigserial,
  codigo_disciplina  character(7) NOT NULL,
  creditos_aula      integer NOT NULL,
  creditos_trabalho  integer NOT NULL,
  carga_horaria      integer NOT NULL,
  ativacao           date NOT NULL,
  desativacao        date,
  nome               varchar(280),
  ementa             text,

  CONSTRAINT pk_disciplina PRIMARY KEY (id_disciplina),
  CONSTRAINT sk_disciplina UNIQUE (codigo_disciplina),

  CONSTRAINT check_creditos_aula
    CHECK (creditos_aula >= 0),
  CONSTRAINT check_creditos_trabalho
    CHECK (creditos_trabalho >= 0),
  CONSTRAINT check_carga_horaria
    CHECK (carga_horaria >= 0)
);

--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS
public.prerequisitos (
  id_disciplina    bigint,
  id_prerequisito  bigint,
  total            boolean,

  CONSTRAINT pk_prerequisito PRIMARY KEY (id_disciplina, id_prerequisito),

	CONSTRAINT fk_disciplina FOREIGN KEY (id_disciplina)
    REFERENCES public.disciplinas (id_disciplina)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
	CONSTRAINT fk_prerequisito FOREIGN KEY (id_prerequisito)
    REFERENCES public.disciplinas (id_disciplina)
      ON DELETE CASCADE
      ON UPDATE CASCADE,

	CONSTRAINT check_prerequisitos
    CHECK (id_disciplina IS DISTINCT FROM id_prerequisito)
);

--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS
public.grade_obrigatoria (
  id_disciplina          bigint,
  ano_grade_obrigatoria  date,

  CONSTRAINT fk_grade_obrigatoria PRIMARY KEY (
    id_disciplina, ano_grade_obrigatoria),

  CONSTRAINT fk_disciplina FOREIGN KEY (id_disciplina)
    REFERENCES public.disciplinas (id_disciplina)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS
public.grade_optativa (
  id_disciplina       bigint,
  ano_grade_optativa  date,
  eletiva             boolean,

  CONSTRAINT fk_grade_optativa PRIMARY KEY (
    id_disciplina, ano_grade_optativa),

  CONSTRAINT fk_disciplina FOREIGN KEY (id_disciplina)
    REFERENCES public.disciplinas (id_disciplina)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS
public.trilhas (
  id_trilha           serial,
  codigo_trilha       character(10) NOT NULL,
  nome                varchar(100) NOT NULL,
  descricao           varchar(280),
  minimo_disciplinas  integer,
  minimo_modulos      integer,

  CONSTRAINT pk_trilha PRIMARY KEY (id_trilha),
  CONSTRAINT sk_trilha UNIQUE (codigo_trilha),

  CONSTRAINT check_minimo_disciplinas
    CHECK (minimo_disciplinas >= 0),
  CONSTRAINT check_minimo_modulos
    CHECK (minimo_modulos >= 0)
);

CREATE TABLE IF NOT EXISTS
public.modulos (
  id_modulo           serial,
  codigo_modulo       varchar(10) NOT NULL,
  minimo_disciplinas  integer,
  id_trilha           integer,

  CONSTRAINT pk_modulo PRIMARY KEY (id_modulo),
  CONSTRAINT sk_modulo UNIQUE (codigo_modulo),

  CONSTRAINT fk_trilha FOREIGN KEY (id_trilha)
    REFERENCES public.trilhas (id_trilha)
      ON DELETE CASCADE
      ON UPDATE CASCADE,

  CONSTRAINT check_minimo_disciplinas
    CHECK (minimo_disciplinas >= 0)
);

--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS
public.optativas_compoem_modulos (
  id_disciplina       bigint,
  ano_grade_optativa  date,
  id_modulo           integer,

  CONSTRAINT pk_composicao PRIMARY KEY (
    id_disciplina, ano_grade_optativa, id_modulo),

  CONSTRAINT fk_grade_optativa FOREIGN KEY (id_disciplina, ano_grade_optativa)
    REFERENCES public.grade_optativa (id_disciplina, ano_grade_optativa)
      ON DELETE RESTRICT
      ON UPDATE CASCADE,
  CONSTRAINT fk_modulo FOREIGN KEY (id_modulo)
    REFERENCES public.modulos (id_modulo)
      ON DELETE RESTRICT
      ON UPDATE CASCADE
);

--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS
public.pessoas (
  id_pessoa   bigserial,
  cpf         character(11),
  nome        varchar(280) NOT NULL,
  nascimento  date,

  CONSTRAINT pk_pessoa PRIMARY KEY (id_pessoa),
  CONSTRAINT sk_pessoa UNIQUE (cpf)
);

CREATE TABLE IF NOT EXISTS
public.administradores (
  id_pessoa           bigint,
  nusp_administrador  varchar(10),
  data_inicio         date NOT NULL,
  data_fim            date,

  CONSTRAINT pk_administrador PRIMARY KEY (id_pessoa, nusp_administrador),

  CONSTRAINT fk_pessoa FOREIGN KEY (id_pessoa)
    REFERENCES public.pessoas (id_pessoa)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS
public.professores (
  id_pessoa       bigint,
  nusp_professor  varchar(10),
  departamento    varchar(100),
  sala            varchar(20),

  CONSTRAINT pk_professor PRIMARY KEY (id_pessoa, nusp_professor),

  CONSTRAINT fk_pessoa FOREIGN KEY (id_pessoa)
    REFERENCES public.pessoas (id_pessoa)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS
public.alunos (
  id_pessoa       bigint,
  nusp_aluno      varchar(10),
  turma_ingresso  date,

  CONSTRAINT pk_aluno PRIMARY KEY (id_pessoa, nusp_aluno),

  CONSTRAINT fk_pessoa FOREIGN KEY (id_pessoa)
    REFERENCES public.pessoas (id_pessoa)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS
public.professores_oferecem_disciplinas (
  id_oferecimento  bigserial,
  id_pessoa        bigint NOT NULL,
  nusp_professor   varchar(10) NOT NULL,
  id_disciplina    bigint NOT NULL,
  ano_semestre     numeric(5, 1) NOT NULL,

  CONSTRAINT pk_oferecimento PRIMARY KEY (id_oferecimento),
  CONSTRAINT sk_oferecimento UNIQUE (
    id_pessoa, nusp_professor, id_disciplina, ano_semestre),

  CONSTRAINT fk_professor FOREIGN KEY (id_pessoa, nusp_professor)
    REFERENCES public.professores (id_pessoa, nusp_professor)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT fk_disciplina FOREIGN KEY (id_disciplina)
    REFERENCES public.disciplinas (id_disciplina)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS
public.alunos_planejam_disciplinas (
  id_plano       bigserial,
  id_pessoa      bigint NOT NULL,
  nusp_aluno     varchar(10) NOT NULL,
  id_disciplina  bigint NOT NULL,
  ano_semestre   numeric(5, 1) NOT NULL,

  CONSTRAINT pk_plano PRIMARY KEY (id_plano),
  CONSTRAINT sk_plano UNIQUE (id_pessoa, nusp_aluno,
                         id_disciplina, ano_semestre),

  CONSTRAINT fk_aluno FOREIGN KEY (id_pessoa, nusp_aluno)
    REFERENCES public.alunos (id_pessoa, nusp_aluno)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT fk_disciplina FOREIGN KEY (id_disciplina)
    REFERENCES public.disciplinas (id_disciplina)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS
public.alunos_cursam_disciplinas (
  id_execucao      bigserial,
  id_pessoa        bigint NOT NULL,
  nusp_aluno       varchar(10) NOT NULL,
  id_oferecimento  bigint NOT NULL,
  nota             numeric(3, 1),

  CONSTRAINT pk_execucao PRIMARY KEY (id_execucao),
  CONSTRAINT sk_execucao UNIQUE (id_pessoa, nusp_aluno, id_oferecimento),

  CONSTRAINT fk_aluno FOREIGN KEY (id_pessoa, nusp_aluno)
    REFERENCES public.alunos (id_pessoa, nusp_aluno)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT fk_oferecimento FOREIGN KEY (id_oferecimento)
    REFERENCES public.professores_oferecem_disciplinas (id_oferecimento)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS
public.administradores_coordenam_grades (
  id_pessoa           bigint,
  nusp_administrador  varchar(10),
  ano_grade           date,
  inicio              date NOT NULL,
  fim                 date,

  CONSTRAINT pk_coordenacao  PRIMARY KEY (
    id_pessoa, nusp_administrador, ano_grade),

  CONSTRAINT fk_administrador FOREIGN KEY (id_pessoa, nusp_administrador)
    REFERENCES public.administradores (id_pessoa, nusp_administrador)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

--------------------------------------------------------------------------------

CREATE OR REPLACE VIEW 
public.professores_completo (
  id_pessoa, 
  nome, 
  cpf, 
  nascimento, 
  nusp_professor, 
  departamento, 
  sala) 
AS
SELECT 
  pessoas.id_pessoa, 
  pessoas.nome, pessoas.cpf, 
  pessoas.nascimento, 
  professores.nusp_professor, 
  professores.departamento, 
  professores.sala 
FROM pessoas INNER JOIN professores 
  ON pessoas.id_pessoa = professores.id_pessoa;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.recupera_disciplina(_codigo_disciplina varchar(7))
  RETURNS TABLE (
    id bigint,
    codigo character (7),
    creditos_aula_disciplina integer,
    creditos_trabalho_disciplina integer,
    carga_horaria_disciplina integer,
    ativacao_disciplina date,
    desativacao_disciplina date,
    nome_disciplina character varying(280),
    ementa_disciplina text
  )  
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
  RETURN QUERY SELECT 
    id_disciplina, 
    codigo_disciplina, 
    creditos_aula, 
    creditos_trabalho, 
    carga_horaria, 
    ativacao, 
    desativacao, 
    nome, 
    ementa 
    FROM disciplinas WHERE codigo_disciplina = _codigo_disciplina;

END
$$;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.prerequisitos_disciplina(_codigo_disciplina varchar(7))
  RETURNS TABLE (
    codigo character (7),
    nome_disciplina character varying(280)
  )  
  LANGUAGE PLPGSQL
  AS
$$
DECLARE
  _id_disciplina bigint;
BEGIN
  _id_disciplina := (select id_disciplina FROM disciplinas WHERE disciplinas.codigo_disciplina = _codigo_disciplina);

  RETURN QUERY SELECT codigo_disciplina, nome FROM disciplinas 
    WHERE id_disciplina IN
    (SELECT id_prerequisito FROM prerequisitos AS p 
      WHERE id_disciplina = _id_disciplina) 
  ;
END
$$;

--------------------------------------------------------------------------------
-- TODO recuperar ano_grade e se é obrigatória ou optativa

CREATE OR REPLACE FUNCTION
public.grade_disciplina(_codigo_disciplina varchar(7))
  RETURNS TABLE (
    ano_grade date,
    eh_eletiva boolean
  )  
  LANGUAGE PLPGSQL
  AS
$$
DECLARE
  _id_disciplina bigint;
BEGIN
  _id_disciplina := (select id_disciplina FROM disciplinas WHERE disciplinas.codigo_disciplina = _codigo_disciplina);
  
  IF (SELECT EXISTS (select id_disciplina from public.grade_obrigatoria where id_disciplina = _id_disciplina)) THEN
    RETURN QUERY SELECT ano_grade_obrigatoria FROM public.grade_obrigatoria
      WHERE id_disciplina = _id_disciplina;
  ELSIF (SELECT EXISTS (select id_disciplina from public.grade_optativa where id_disciplina = _id_disciplina)) THEN
    RETURN QUERY SELECT ano_grade_optativa, eletiva FROM public.grade_optativa
      WHERE id_disciplina = _id_disciplina;
  ELSE
    RETURN;
  END IF;

END
$$;


--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION 
public.cria_nova_disciplina3
(
  _codigo_disciplina varchar(7),
  _creditos_aula integer,
  _creditos_trabalho integer,
  _carga_horaria integer,
  _ativacao date,
  _nome character varying(280),
  _ementa text,
  _ano_grade date,
  _obrigatoria boolean,
  _eletiva boolean,
  _prerequisitos text
)
RETURNS void
LANGUAGE PLPGSQL
AS
$$
DECLARE   
  Counter INT = 0 ; 
  _id_disciplina bigint;
  _lista_prerequisitos varchar(7)[];
  _id_prerequisito bigint;
BEGIN 

  INSERT INTO
  public.disciplinas(
    codigo_disciplina,
    creditos_aula,
    creditos_trabalho,
    carga_horaria,
    ativacao,
    nome,
    ementa)
  VALUES (
    _codigo_disciplina, 
    _creditos_aula, 
    _creditos_trabalho, 
    _carga_horaria, 
    _ativacao, 
    _nome, 
    _ementa);

  _id_disciplina := (select id_disciplina FROM disciplinas WHERE disciplinas.codigo_disciplina = _codigo_disciplina);

  IF _obrigatoria THEN
    INSERT INTO public.grade_obrigatoria(
      id_disciplina,
      ano_grade_obrigatoria
      )
    VALUES (
      _id_disciplina,
      _ano_grade
      );
  ELSE
    INSERT INTO public.grade_optativa(
      id_disciplina,
      ano_grade_optativa,
      eletiva
      )
    VALUES (
      _id_disciplina,
      _ano_grade,
      _eletiva
      );
  END IF;

  _lista_prerequisitos = string_to_array(_prerequisitos,',');

  IF array_length(_lista_prerequisitos, 1) > 0 THEN

    FOR Counter in array_lower(_lista_prerequisitos, 1) .. array_upper(_lista_prerequisitos, 1)
    LOOP
    _id_prerequisito := (SELECT id_disciplina FROM disciplinas WHERE disciplinas.codigo_disciplina = _lista_prerequisitos[Counter]);

      INSERT INTO public.prerequisitos(
        id_disciplina,
        id_prerequisito,
        total) 
      VALUES (
        _id_disciplina,
        _id_prerequisito,
        True); 
    END LOOP;
    
  END IF;

 END
$$;

-- select cria_nova_disciplina3('MACvazi', 5, 1, 39, '2012-02-03', 'Aeeeew', 'Ementaaa', '2012-01-01', false, true, 'MAC0350,MAC0101,MAC0110,MAC0218');
-- select cria_nova_disciplina3('MACsem', 5, 1, 39, '2012-02-03', 'Aeeeew', 'Ementaaa', '2012-01-01', false, true, '');
-- select cria_nova_disciplina3('MACuma', 5, 1, 39, '2012-02-03', 'Aeeeew', 'Ementaaa', '2012-01-01', false, true, 'MAC0209');

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION 
public.atualiza_disciplina
(
  _id_disciplina bigint,
  _codigo_disciplina varchar(7),
  _creditos_aula integer,
  _creditos_trabalho integer,
  _carga_horaria integer,
  _ativacao date,
  _nome character varying(280),
  _ementa text,
  _ano_grade date,
  _obrigatoria boolean,
  _eletiva boolean,
  _prerequisitos text,
  _desativacao date default NULL
)
RETURNS void
LANGUAGE PLPGSQL
AS
$$
DECLARE   
  Counter INT = 0 ; 
  _lista_prerequisitos varchar(7)[];
  _id_prerequisito bigint;
BEGIN 

  IF (_desativacao = NULL) THEN
    UPDATE public.disciplinas SET (
      codigo_disciplina,
      creditos_aula,
      creditos_trabalho,
      carga_horaria,
      ativacao,
      nome,
      ementa)
    = (
      _codigo_disciplina, 
      _creditos_aula, 
      _creditos_trabalho, 
      _carga_horaria, 
      _ativacao, 
      _nome, 
      _ementa)
    WHERE id_disciplina = _id_disciplina;
  ELSE
    UPDATE public.disciplinas SET (
      codigo_disciplina,
      creditos_aula,
      creditos_trabalho,
      carga_horaria,
      ativacao,
      desativacao,
      nome,
      ementa)
    = (
      _codigo_disciplina, 
      _creditos_aula, 
      _creditos_trabalho, 
      _carga_horaria, 
      _ativacao, 
      _desativacao,
      _nome, 
      _ementa)
    WHERE id_disciplina = _id_disciplina;
  END IF;

  IF _obrigatoria THEN
    DELETE FROM public.grade_optativa WHERE id_disciplina = _id_disciplina;
    -- confere se já era obrigatória, se for atualiza, se não insere
    IF (SELECT EXISTS (select id_disciplina from public.grade_obrigatoria where id_disciplina = _id_disciplina)) THEN
      UPDATE public.grade_obrigatoria SET ano_grade_obrigatoria = _ano_grade WHERE id_disciplina = _id_disciplina;
    ELSE
      INSERT INTO public.grade_obrigatoria(
        id_disciplina,
        ano_grade_obrigatoria
        )
      VALUES (
        _id_disciplina,
        _ano_grade
        );
    END IF;
  ELSE
    DELETE FROM public.grade_obrigatoria WHERE id_disciplina = _id_disciplina;
    -- confere se já era optativa, se for atualiza, se não insere
    IF (SELECT EXISTS (select id_disciplina from public.grade_optativa where id_disciplina = _id_disciplina)) THEN
      UPDATE public.grade_optativa SET (
        ano_grade_optativa,
        eletiva
        )
      = (
        _ano_grade,
        _eletiva)
      WHERE id_disciplina = _id_disciplina;
    ELSE
      INSERT INTO public.grade_optativa(
        id_disciplina,
        ano_grade_optativa,
        eletiva
        )
      VALUES (
        _id_disciplina,
        _ano_grade,
        _eletiva
        );
    END IF;
  END IF;

  DELETE FROM public.prerequisitos WHERE id_disciplina = _id_disciplina;
  _lista_prerequisitos = string_to_array(_prerequisitos,',');

  IF array_length(_lista_prerequisitos, 1) > 0 THEN

    FOR Counter in array_lower(_lista_prerequisitos, 1) .. array_upper(_lista_prerequisitos, 1)
    LOOP
    _id_prerequisito := (SELECT id_disciplina FROM disciplinas WHERE disciplinas.codigo_disciplina = _lista_prerequisitos[Counter]);

      INSERT INTO public.prerequisitos(
        id_disciplina,
        id_prerequisito,
        total) 
      VALUES (
        _id_disciplina,
        _id_prerequisito,
        True); 
    END LOOP;
    
  END IF;

 END
$$;

-- select atualiza_disciplina(40, 'MACvazi', 7, 7, 7, '2012-02-03', 'Aeeeew', 'Ementaaa atualizada', '2012-01-01', false, true, 'MAC0350,MAC0101,MAC0110,MAC0218,MAC0426,MAC0209', NULL);
-- select atualiza_disciplina(40, 'MACivaz', 7, 8, 9, '2012-02-03', 'Aeew', 'Ementa atualizadísima', '2018-01-01', true, false, 'MAC0350,MAC0101,MAC0110,MAC0218,MAC0426,MAC0209', NULL);
-- select atualiza_disciplina(40, 'MACivaz', 3, 4, 5, '2012-02-03', 'Aeeeew', 'atualizadísima', '2018-01-01', true, false, 'MAC0218,MAC0426,MAC0209', NULL);

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.exclui_disciplina(_codigo_disciplina varchar(7))
  RETURNS void
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
  DELETE FROM public.disciplinas WHERE codigo_disciplina = _codigo_disciplina;
END
$$;
