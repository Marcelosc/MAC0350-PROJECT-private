import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
})

export default () => {
  return new Vuex.Store({
    plugins: [vuexLocal.plugin],

    state: {
      email: '',
      token: '',
      disciplina_selecionada: '',
    },

    getters: {
      isAuthenticated(state) {
        return state.token !== '';
      },
      getToken(state) {
        return state.token;
      },
      getEmail(state) {
        return state.email;
      },
      getDisciplina_selecionada(state) {
        return state.disciplina_selecionada;
      }
    },

    mutations: {
      setEmail(state, { email }) {
        state.email = email;
      },
      setToken(state, { token }) {
        state.token = token;
      },
      setDisciplina_selecionada(state, { disciplina_selecionada }) {
        state.disciplina_selecionada = disciplina_selecionada;
      }
    },

    actions: {
      async login({ commit }, email, password ) {
        // TODO: Implement login logic here
        // TIP: Method is `async` because it should use "await this.$axios"
        console.log(`Tentando login ...`)
        // console.log(`User: ${email} ${password}`);
        console.log("login email: " + email);
        console.log("login password: " + password);
        // console.log("login state.email: " + state.email);
        // console.log("login state.password: " + state.password);

        // console.log("login state.message: " + state.message);

        try {
          const token = await this.$axios.$post("/rpc/autenticar", {
          // const token = await this.$axios.$post(`http://localhost:3000/rpc/autenticar`, {
          // const token = await this.$axios.post(`POST /localhost:3000/rpc/autenticar`, {
          // const token = await app.$axios.post(`/rpc/autenticar`, {
          // const token = await app.$axios.$post(`/rpc/autenticar`, {
            // email: email,
            // senha: password
            email: "jef@ime.usp.br",
            senha: "12345"
          });
          console.log(`token: ${token}`);
          

          commit(setEmail, {email: email});
          commit(setToken, {token: token});


          console.log("login state.email: " + state.email);
          console.log("login state.password: " + state.password);
          
          // this.$store.commit("setToken", { token: "" });
          // console.log("token jwt: " + this.$store.getters.getToken)

        } catch (err) {
          console.log('Erro: email ou senha inválidos');
          // document.getElementById("demo").innerHTML = err.message;
          // console.log(err.printStackTrace());
          console.log(err.message);
          err.printStackTrace();
        }

        // Save token at global vuex store to be used by auth middleware
      },

      async logout({commit}) {
        //Definindo como vazio o token JWT no cabeçalho padrão das requisiões
        this.$axios.defaults.headers.common['Authorization'] = ``;
        commit(setEmail, {email: ""});
        commit(setToken, {token: ""});
        // this.$store.commit("setToken", { token: '' }); //TODO remover
      },

      async hasPermission({ state }) {
        // this.$axios.defaults.headers.common['Authorization'] = 
        //   `Bearer ${state.token}`; //TODO talvez fosse melhor fazer isso no login
        
        this.$axios.defaults.headers.common['Authorization'] = `Bearer ${state.token}`; //TODO talvez fosse melhor fazer isso no login
        // axios.defaults.headers.common['authorization'] = `Bearer ${token}`;

        return await this.$axios.$post("/rtc/autorizar", {
          caminho_servico: `${route.fullPath}`
        });
      }
    }
  });
}
